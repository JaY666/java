package com.sda.impl;

import com.sda.api.Item;

public class IteamImpl implements Item {

	private String name;
	private int quantity;
	public IteamImpl(String name, int quantity) {
		this.name = name;
		this.quantity = quantity;
	}

	
	
	
	public void setName(String name) {
		this.name = name;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	@Override
	public String getName() {
		
		return name;
	}

	@Override
	public int getQuantity() {
		return quantity;
	}

}
