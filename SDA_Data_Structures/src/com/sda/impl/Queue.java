package com.sda.impl;

import java.util.ArrayList;
import java.util.List;

import com.sda.api.Customer;
import com.sda.api.Structure;

public class Queue implements Structure<Customer>{

	private List<Customer> listOfCustomers = new ArrayList<>();
	
	
	@Override
	public void push(Customer item) {
		listOfCustomers.add(item);	
	}

	@Override
	public Customer pop() {
		Customer customerToPop = listOfCustomers.get(0);
		listOfCustomers.remove(0);
		return customerToPop;
	}

	@Override
	public Customer peek() {
		return listOfCustomers.get(0);
	}

	@Override
	public void printStructure() {
		
		for (Customer customer : listOfCustomers) {
		
			
		System.out.println("First to out: " + customer.getName());
			
		
		System.out.println(customer.getName());
		}
	}

}
