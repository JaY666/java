package pl.sdacademy.main;

public class SimpleEx {

	public int addElementsDividedBySeven(int[][] arr) {

		int sum = 0;

		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {

				if (arr[i][j] % 7 == 0) {
					sum += arr[i][j];
				}
			}
		}

		return sum;
	}

	public int multiplyElements(int[][] arr) {

		int sum = 1;

		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				sum = sum * arr[i][j];

			}
		}

		return sum;
	}

	public int multiplyEvenElements(int[][] arr) {

		int sum = 1;

		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				if (arr[i][j] % 2 == 0) {
					sum = sum * arr[i][j];
				}

			}
		}

		return sum;

	}

	public int multiplyOddElements(int[][] arr) {

		int sum = 1;

		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				if (arr[i][j] % 2 != 0) {
					sum = sum * arr[i][j];
				}

			}
		}

		return sum;

	}

	public int multiplyElementsDividedByThree(int[][] arr) {

		int sum = 1;

		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				if (arr[i][j] % 3 == 0) {
					sum = sum * arr[i][j];
				}

			}
		}

		return sum;

	}

	public int find2DArrayMin(int[][] arr) {

		int max = Integer.MAX_VALUE;

		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				if (arr[i][j] < max) {
					max = arr[i][j];
				}

			}
		}

		return max;

	}

	public int find2DArrayMax(int[][] arr) {

		int min = Integer.MIN_VALUE;

		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				if (arr[i][j] > min) {
					min = arr[i][j];
				}

			}
		}

		return min;

	}

	public int[] sumEvenNumbersInRows(int[][] arr) {

		int[] result = new int[arr.length];

		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				if (arr[i][j] % 2 == 0) {
					result[i] += arr[i][j];
				}

			}
		}

		return result;

	}
	
	
	public void printZadanie8 (int[] arr) {
		
		for (int i =0; i < arr.length; i++){
			System.out.println(arr[i]);
		}
		
		
		
	}
	
	
	
	

	public void print2DArray(int[][] m) {

		for (int i = 0; i < m.length; i++) {
			for (int j = 0; j < m[i].length; j++) {

				System.out.print(m[i][j] + "  ");

			}
			System.out.println();
		}
	}

	
	public int[][] identityMatrix(int[][] m) {
		
		int[][] result = new int[m.length][];
		
		
		
		
		
		return result;
	}
	
	
	
	
	
	
	
	
	
}
