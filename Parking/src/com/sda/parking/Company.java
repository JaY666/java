package com.sda.parking;

import java.util.ArrayList;

public class Company {
	
	private ArrayList<Parking> parkings;
	private CarRegistry registry;
	
	public Company() {
	parkings = new ArrayList<>();
	registry = new CarRegistry();
}
	
	public void addParking() {
		parkings.add(new Parking());
		System.out.println("Dodano parking: " + (parkings.size() -1));
	}
	
	
	public Parking getParking (int id) {
		return parkings.get(id);
	}
	
	
	public void addCar (int parkingId) {
		parkings.get(parkingId).addCar();
	}
	
	public void carEntry(int parkingId) {
		String ticketId = parkings.get(parkingId).carEntry();
		registry.addToRegistry(ticketId);		
		
	}
	
}
