package com.sda.parking;

import java.util.HashMap;
import java.util.Map;

public class CarRegistry {

	
	private Map<String, ParkingInformation> registry;
	
	
	public CarRegistry() {
		registry = new HashMap<>();
		
	}
	
	public void addToRegistry(String tickedID) {
		registry.put(tickedID, new ParkingInformation(tickedID));
		System.out.println("Dodano ticket ID " + tickedID + " do rejestru" );
	}
	
	
	
	
}
