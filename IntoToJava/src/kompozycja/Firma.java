package kompozycja;

import java.util.ArrayList;
import java.util.List;

public class Firma {

	private String nazwa;
	private List<Pracownik> pracownicy;
	private TypFirmy typfirmy;
	
	public Firma(String nazwa, TypFirmy typfirmy) {
		this.nazwa = nazwa;
		this.pracownicy = new ArrayList<>();
		this.typfirmy = typfirmy;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public List<Pracownik> getPracownicy() {
		return pracownicy;
	}

	public void setPracownicy(List<Pracownik> pracownicy) {
		this.pracownicy = pracownicy;
	}

	public TypFirmy getTypfirmy() {
		return typfirmy;
	}

	public void setTypfirmy(TypFirmy typfirmy) {
		this.typfirmy = typfirmy;
	}
	
	public void add (Pracownik pracownik) {
		pracownicy.add(pracownik);
	}
	
}
