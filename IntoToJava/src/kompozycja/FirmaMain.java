package kompozycja;

public class FirmaMain {
	
	public static void main (String[] args) {
		
		Firma firma = new Firma("Super Firma", TypFirmy.SPOLKA_ZOO);
		Pracownik pracownik = new Pracownik("Jan", "Kowalski", new Adres("Kwiatowa","Gdansk",4));
		Pracownik pracownik1 = new Pracownik("Jan", "Kowalski", new Adres("Kwiatowa","Gdansk",4));
		firma.add(pracownik);
		firma.add(pracownik1);
		
		show(firma);
		
		
	}
	
	
	public static void show (Firma firma) {
		System.out.println("Firma " + firma.getNazwa()+ " " + firma.getTypfirmy());
		
		for (Pracownik pracownik: firma.getPracownicy()) {
			show(pracownik);
		}
		
		
	}
	
	public static void show (Pracownik pracownik) {
		System.out.print("pracownik " + pracownik.getImie() + " " + pracownik.getNazwisko());
		show(pracownik.getAdres());
	}
	
	public static void show (Adres adres) {
	System.out.println("adres " + adres.getMiasto()+ " " +adres.getUlica()+ " " +adres.getNumerDomu());
	
}
	
	
	
	
	
	

}
