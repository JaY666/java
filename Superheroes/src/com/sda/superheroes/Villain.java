package com.sda.superheroes;

public class Villain extends AbstractHero {

	public Villain(String name, TeamType team, HeroStatistics stats) {
		super(name, team, stats);
		// TODO Auto-generated constructor stub
	}

	@Override
	public
	double getPower() {
		return ( this.getStats().getAttack() + this.getStats().getHealth() ) * this.getStats().getDefense();
	}

}
