package com.sda.superheroes;

public abstract class AbstractHero {
	
	private String name;
	private TeamType team;
	private HeroStatistics stats;
	
	public abstract double getPower();
	
	public AbstractHero(String name, TeamType team, HeroStatistics stats) {
		this.name = name;
		this.team = team;
		this.stats = stats;
		
		
		switch (team) {
		  case RED :
		    stats.addToHealth(50);
				break;  
		  case BLUE :
			stats.addToAttack(50);
				break;  
		  case GREEN :
		    stats.addToDefense(50);
			  break;				  
		}	
		
	}
	
	
	
	
	
	

	public String getName() {
		return name;
	}

	public TeamType getTeam() {
		return team;
	}

	public HeroStatistics getStats() {
		return stats;
	}
	

		


}
