package com.sda.superheroes;

public class SuperHero extends AbstractHero {

	public SuperHero(String name, TeamType team, HeroStatistics stats) {
		super(name, team, stats);
		// TODO Auto-generated constructor stub
	}

	@Override
	public
	double getPower() {
		return ( this.getStats().getAttack() + this.getStats().getDefense() ) * this.getStats().getHealth();
	}

}
