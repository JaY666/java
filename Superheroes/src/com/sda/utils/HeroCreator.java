package com.sda.utils;

import com.sda.superheroes.HeroStatistics;
import com.sda.superheroes.SuperHero;
import com.sda.superheroes.TeamType;
import com.sda.superheroes.Villain;

public abstract class HeroCreator {

	
	
	public static SuperHero createSuperHero(String name, HeroStatistics stats, TeamType team) {	
		return new SuperHero(name, team, stats);
		
	}
	
	
	public static Villain createVillain(String name, HeroStatistics stats, TeamType team) {
		return new Villain(name, team, stats);
		
	}
	
	
	 public static SuperHero createHeroWithDefaultStats(String name, TeamType team){
		 PropertyReader.loadPropertyValues();
		 
		 Integer defaultHealth = Integer.parseInt(System.getProperty("config.superHeroBaseHealth"));
		 Integer defaultAttack = Integer.parseInt(System.getProperty("config.superHeroBaseAttack"));
		 Integer defaultDefence = Integer.parseInt(System.getProperty("config.superHeroBaseDefence"));
		 
		 return new SuperHero(name, team, new HeroStatistics(defaultHealth, defaultAttack, defaultDefence) );
		 }
	 
	 public static Villain createVillainWithDefaultStats(String name, TeamType team){
		 PropertyReader.loadPropertyValues();
		 
		 Integer defaultHealth = Integer.parseInt(System.getProperty("config.VillainBaseHealth"));
		 Integer defaultAttack = Integer.parseInt(System.getProperty("config.VillainBaseAttack"));
		 Integer defaultDefence = Integer.parseInt(System.getProperty("config.VillainBaseDefence"));
		 
		 return new Villain(name, team, new HeroStatistics(defaultHealth, defaultAttack, defaultDefence) );
		 }
	
	
}
