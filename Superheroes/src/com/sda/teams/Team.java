package com.sda.teams;

import java.util.ArrayList;
import java.util.List;

import com.sda.superheroes.AbstractHero;
import com.sda.superheroes.TeamType;

public class Team {
	
	TeamType team;
	List<AbstractHero> heroesList;
	private AbstractHero teamLeader;
	
	public Team(TeamType team) {
		this.team = team;
		heroesList = new ArrayList<AbstractHero>();
	}

	public boolean addHeroToTeam(AbstractHero hero) {
			
		if (hero.getTeam() == team) {
			heroesList.add(hero);
			findTeamLeader();
			return true;
		} else {
			
			System.out.println("You can't add your hero to this team");
			return false;
			
		}
	
	}

	@Override
	public String toString() {
		
		
		StringBuilder sb = new StringBuilder();
		
		for ( AbstractHero ah : heroesList) {
			
			sb.append(ah.getName());
			
			
			if (  ah.getName().equals(teamLeader.getName()) & ah.getPower() == teamLeader.getPower() ) {
			sb.append("<<LEADER");
			
		}
			sb.append("\n");
		}
		return sb.toString();
	}

	public AbstractHero findTeamLeader() {
		
		
		int maxPower = 0;
		for ( int i = 0; i < heroesList.size(); i++) {
			
			if (heroesList.get(i).getName().equals("Batman")) {
			
				return teamLeader = heroesList.get(i);
				
			} else if (heroesList.get(i).getPower() > maxPower ) {
				maxPower = (int) heroesList.get(i).getPower();
				teamLeader = heroesList.get(i);
			
			} 
		
		}
		return teamLeader;	
	}

	public AbstractHero getTeamLeader() {
		return teamLeader;
	}
	
	
}
