package com.sda.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import com.sda.superheroes.HeroStatistics;
import com.sda.superheroes.SuperHero;
import com.sda.superheroes.TeamType;
import com.sda.superheroes.Villain;

public class AbstractHeroTest {
	
	
	
	@Test
	public void heroTest() {
		
		Villain vi = new Villain("Tomek", TeamType.RED , new HeroStatistics(100, 100, 100));
		SuperHero sh = new SuperHero("Andrzej", TeamType.GREEN, new HeroStatistics(100, 100, 100));
		
		assertTrue(vi.getStats().getAttack() ==100);
		assertTrue(vi.getStats().getDefense() ==100);
		assertTrue(vi.getStats().getHealth() ==150);
		
		assertTrue(sh.getStats().getAttack() ==100);
		assertTrue(sh.getStats().getDefense() ==150);
		assertTrue(sh.getStats().getHealth() ==100);
		
		
		
		
		
	}

}
