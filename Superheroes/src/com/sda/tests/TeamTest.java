package com.sda.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import com.sda.superheroes.HeroStatistics;
import com.sda.superheroes.SuperHero;
import com.sda.superheroes.TeamType;
import com.sda.superheroes.Villain;
import com.sda.teams.Team;
import com.sda.utils.HeroCreator;

public class TeamTest {
	
	
	
	
	
	@Test
	public void teamTest() {
		
		Team team = new Team(TeamType.RED);
		
		
		
		assertTrue(team.addHeroToTeam(HeroCreator.createHeroWithDefaultStats("Tomek", TeamType.RED)));
		assertFalse(team.addHeroToTeam(HeroCreator.createVillainWithDefaultStats("Rysiek", TeamType.BLUE)));
		assertTrue (team.addHeroToTeam(HeroCreator.createVillainWithDefaultStats("Andrzej", TeamType.RED)));
		
		assertTrue(team.findTeamLeader().getName().equals("Tomek"));
		
		assertTrue(team.addHeroToTeam(HeroCreator.createSuperHero("Maciek", new HeroStatistics(200, 200, 200), TeamType.RED)));
		assertTrue(team.findTeamLeader().getName().equals("Maciek"));
				
		assertTrue (team.addHeroToTeam(HeroCreator.createVillainWithDefaultStats("Batman", TeamType.RED)));
		assertTrue(team.findTeamLeader().getName().equals("Batman"));
		
		
		System.out.println(team.toString());
	
	}

	
	

}
