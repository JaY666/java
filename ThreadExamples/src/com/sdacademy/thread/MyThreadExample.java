package com.sdacademy.thread;

public class MyThreadExample extends Thread {
	
	private int number = 0;
	
	public MyThreadExample() {}
	
	public MyThreadExample(int number) {
		this.number = number;
	}
	
	
	@Override
	public void run() {
		System.out.println("Twoja liczba to: " +number);
	}

}
