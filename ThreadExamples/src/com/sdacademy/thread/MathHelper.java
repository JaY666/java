package com.sdacademy.thread;

import java.math.BigInteger;
import java.util.LinkedList;
import java.util.List;

public class MathHelper {
	
	public static boolean isPrime(long n) {
		
		
		for ( int i  = 2; i < Math.sqrt(n); i++) {
			
			
			if ( n % i == 0) {
				
				return false;
	
		}	
		
	}
		return true;
	}
	
	public static boolean isPrimeWhile(long n) {
		
		
		int i = 2;
		while ( i < Math.sqrt(n)) {
			
			if ( n % i == 0) {
				
				return false;
	
		}	
			
			i++;
		}
	
		
		return true;

	}
	
	
	
	public static boolean isBigPrime(BigInteger n){
		
		BigInteger i = new BigInteger("2");
		BigInteger m = new BigInteger("0");
		while ( i.compareTo(n) == -1) {
			
			if ( n.mod(i).equals(m)) {
				
				return false;
	
		}	
			
			i = i.add(BigInteger.ONE);
		}
	
		
		return true;
	}
	
	public static List<String> ArraysOfPrime (List<String> input){
		
		List<String> output = new LinkedList<>();
		
		for (String s : input) {
			long start = System.currentTimeMillis();
			MathHelper.isPrimeWhile(Long.valueOf(s));
			long stop = System.currentTimeMillis();
			output.add("Wywowalnie " + s + " zajelo: " + (stop - start) + " ms");
		}
	
		return output;
				}
	
	public static List<String> BigArraysOfPrime (List<String> input) {
		List<String> output = new LinkedList<>();
		
		for (String s : input) {
			long start = System.currentTimeMillis();
			MathHelper.isBigPrime(new BigInteger(s));
			long stop = System.currentTimeMillis();
			output.add("Wywowalnie " + s + " zajelo: " + (stop - start) + " ms");
		}
	
		return output;
	}
	
	
	
	

}
