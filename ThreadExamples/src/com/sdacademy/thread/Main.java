package com.sdacademy.thread;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Main {

	public static void main(String[] args) {


		
//		Thread t = new Thread(new MyThreadExample(5), "Moj watek");
//		t.start();
//		
//		
//		new Thread(new OtherThread(), "inny watek").start();
//		
//		
//		t.setPriority(Thread.MAX_PRIORITY);
//		
//		Thread t2 = new Thread(() -> System.out.println("Trzeci watek."));
//		t2.start();
//		
////		t2.setDaemon(true);
//		
//		System.out.println("Koniec");
//		
//		
//		System.out.println(Thread.activeCount());
//		
//		BigInteger b = new BigInteger("1000000000000000001");
//		b = b.add(BigInteger.TEN);
//		System.out.println(b.toString());
//		
//		System.out.println(MathHelper.isPrime(13));
		
		
		
//		long start = System.currentTimeMillis();
//		System.out.println("Liczba 858,599,509 jest liczba pierwsza: " + MathHelper.isPrimeWhile(858599509));
//		
//		
//		System.out.println("Wykonanie zadania zajelo: "+ (System.currentTimeMillis() - start) + "ms");
		
//		
//		BigInteger x = new BigInteger("3");
//		
//		BigInteger d1 = new BigInteger("2");
//		d1 = d1.add(x);
//		System.out.println("2 + x = " + d1.toString() );
//		
//		
//		System.out.println("2x + 5 = " + x.multiply(BigInteger.valueOf(2)).add(BigInteger.valueOf(5)));
//			
//		System.out.println("4x2 + 5x + 6 = "+ x.pow(2).multiply(BigInteger.valueOf(4)).add(BigInteger.valueOf(5).multiply(x).add(BigInteger.valueOf(6))) );
//		
//		System.out.println("9 - 15%x = "+ BigInteger.valueOf(9).subtract(BigInteger.valueOf(15).mod(x)) );
//		
//		System.out.println("7/2 + 3 = "+ BigInteger.valueOf(7).divide(BigInteger.valueOf(2)).add(BigInteger.valueOf(3)));
//		
//		System.out.println("(x%3) * (x%4) + 5 = " + x.mod(BigInteger.valueOf(3)).multiply(x.mod(BigInteger.valueOf(4))).add(BigInteger.valueOf(5)));
//			
//		System.out.println("[x3 + (3x/x^2) + 7 - x2]%x = "+ (x.pow(3).add(x.multiply(BigInteger.valueOf(3).divide(x.pow(2))).add(BigInteger.valueOf(7)).subtract(x.pow(2)).mod(x))));

	
//		long start = System.currentTimeMillis();
//		
//		System.out.println("Liczba 49979693 jest pierwsza: " + MathHelper.isBigPrime(BigInteger.valueOf(49979693)));
//		
//		long stop = System.currentTimeMillis();
//		
//		System.out.println("Wywowalnie zajelo: " + (stop - start) + " ms");


		
		 
		
		
//		System.out.println(MathHelper.BigArraysOfPrime(input).toString());
		
		
		List<String> input = Arrays.asList("49979693","198491329","314606891","393342743","553105243","715225739","899809343","982451000653");
		
		
		
		for( int i = 0; i < input.size(); i=i+2){
			
			final String in = input.get(i);
			
			new Thread(() -> MathHelper.isBigPrime( new BigInteger(in))).start();			
			if(input.size() % 2 == 0 || i != input.size()) {
				final String in2 = input.get(i);
				
				new Thread(() -> MathHelper.isBigPrime( new BigInteger(in2))).start();
			}
			
			
			
		}
		
		for( int i = 1; i < input.size(); i=i+2){
			
			
		}
		
	}
	



}
