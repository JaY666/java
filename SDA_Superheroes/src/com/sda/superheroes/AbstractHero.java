package com.sda.superheroes;

import com.sda.teams.TeamType;

public abstract class AbstractHero {
	
	private String name;
	private HeroStatistics stats;
	private TeamType team;

	/*
	 * constructor goes here
	 */

	public abstract double getPower();
	
}
