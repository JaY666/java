package object2;

public class ObjectSeriesMain {

	public static void main(String[] args) {

		
		Serie serie = new SimpleSerie();
		System.out.println(serie.sum(5));
		
		
		Serie constantSerie = new Serie() {
			
			@Override
			public int generate(int i) {
				return 5;
			}
		};
		
		System.out.println(constantSerie.sum(5));
		
		Serie someSerie = new SomeSeries();
		System.out.println(someSerie.sum(5));
		
	}

	static class SomeSeries extends Serie {

		@Override
		public int generate(int i) {
			return i+10;
		}
		
	}
	
	
	
	
	
	
}
