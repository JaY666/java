package stream;

import java.util.Arrays;

import java.util.List;

import java.util.stream.Collectors;

public class Zad1 {
	
	public static boolean isLessThanTen (int n) {
		return n<10;
	}
	
public static void main(String[] args) {
	
	
	
	List<Integer> numbers = Arrays.asList(1,25,30,31,23,15,16,3,5,30);
	
	int sum = numbers.stream().filter( (n) -> (n<10)).reduce((a,b) -> a+b).get();
	System.out.println(sum);
	
	int max = numbers.stream().reduce(Math::max).get();
	System.out.println(max);
	
	
	//numbers.stream().map((n) -> (n<10)).forEach(System.out::println);
	
	
	
	
	
//	List<Integer> lessThanTen = numbers.stream()
//		.filter( Zad1::isLessThanTen)
//		.collect(Collectors.toList());
//	
//		lessThanTen.forEach(System.out::println);
//		
		
//		List<Integer> divisibleByThree = numbers.stream()
//				.filter( (n) -> (n%3==0))
//				.collect(Collectors.toList());
//			
//		divisibleByThree.forEach(System.out::println);
		
		List<Integer> lastNumberIsFive = numbers.stream()
				.filter( (n) -> (n%10==5))
				.collect(Collectors.toList());
			
		lastNumberIsFive.forEach(System.out::println);
		
		
		
		
	
	
	//numbers.stream().forEach(System.out::println);
	

	//numbers.stream().filter( (n) -> (n<10)).forEach(System.out::println);
	
//	numbers.stream().filter( (n) -> (n%3==0)).forEach(System.out::println);
//	
//	numbers.stream().filter( (Integer i) -> {
//		int reszta = i%3;
//		boolean czyZero = (reszta==0);
//		return czyZero;
//	
//	}).forEach(System.out::println);
	
//	numbers.stream().filter( (n) -> (n%10==5)).forEach(System.out::println);
	
//	numbers.stream().filter((Integer i) -> {
//		String text = i.toString();
//		char lastChar = text.charAt(text.length()-1);
//		return lastChar == '5';
//		
//	}).forEach(System.out::println);
	
	
}
}
