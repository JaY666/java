package Zad5;

public abstract class FamilyMember {
	
	private String name;
	
	
	public FamilyMember (String name) {
		this.name = name;
		System.out.println("tworz� cz�onka rodziny");
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
		
	}
	
	public abstract void introduceYoursef();

}
