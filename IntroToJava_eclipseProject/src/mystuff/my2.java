package mystuff;

import java.util.Scanner;

public class my2 {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		
		System.out.println("What is input unit? (f/c)");
		String line = scan.nextLine();
		if (line.equals("f")) {
			System.out.println("Please input temp in Fahrenheit: ");
			
			double fahrenheitTemp = scan.nextDouble();
			double celsiusTemp = (fahrenheitTemp - 32)*5/9;

			System.out.println("celsius: "+ celsiusTemp);
			
		} else if (line.equals("c")) {
			System.out.println("Please input temp in Celsiuses: ");
			
			double celsiusTemp = scan.nextDouble();
			double fahrenheitTemp = celsiusTemp*9/5+32;
			System.out.println("fahrenheit: "+ fahrenheitTemp);
		}
	}

}