package mystuff;

public class my8 {
	
	private static void suma(int a, int b) {
		
		int wynik = a + b;
		System.out.println(wynik);
		
	}

	private static int mnozenie (int a, int b) {
		
		int wynik = a * b;
		return wynik;
		
	}
	
	private static int min (int[] tab) {
		
		int m = Integer.MAX_VALUE;
		
		for( int i=0; i<tab.length; i++) {
			if (m>tab[i]) {
				
				m = tab[i];
				
			}
			
		}
		
		return m ;
	}
	
	private static int sum(int[] tab) {
		int s = 0;
		for (int i: tab) {
			s+= i;
		}
		return s;
	}
	
	private static int iloczyn(int[] tab) {
		int s = 1;
		for (int i: tab) {
			s = s*i;
		}
		return s;
		
	}
	
	private static int znaki(String text, char znak){
				
		char[] znaki = text.toCharArray() ;
		
		int ilosc = 0 ;
			
		for (char z : znaki) {
			if (z==znak){
			ilosc++;
		}
		}
			return ilosc;
		
	}
	
	private static double poleKwadratu (double a) {
		
		return a*a;	
	}
	
	private static double poleSzescianu (double a) {
		
		return 6*poleKwadratu(a);
	}
	
	private static double poleKola (double r) {
		
		return r*r*Math.PI;
	}
	
	private static double objetoscWalca (double r, double h) {
		
		return poleKola(r)*h;
		
	}
	
	private static double objetoscStozka (double r, double h) {
		
		return objetoscWalca(r,h)/3;
	}
	
	private static double objetoscOstroslupaKwadrat (double a, double h ) {
		
		return poleKwadratu(a)*h/3;
		
	}
	
	private static void show (int i) {}
	
	private static void show (double i) {}
	
	private static void show (String text) {}
	
	
	public static void main(String[] args) {
		
		//suma(3,4);
		
		//System.out.println(mnozenie (4, 7));
		
		//suma(mnozenie(3,4),10);
		
		//int[] tab = {2,34,3,2,1,11};
        //int tabMin = min(tab);
        //System.out.println("tabMin="+tabMin);
		
		//int[] liczby = {1,4,11,4};
		//System.out.println("Suma :" + sum(liczby));
		
		//int[] liczby = {1,2,2,2};
		//System.out.println("Suma :" + iloczyn(liczby));
		
		//System.out.println(znaki("Kuba",'a'));
        
        //System.out.println(poleKwadratu(4));
		
		//System.out.println(objetoscStozka(3,4));
		
		//System.out.println(poleSzescianu(2));
		
		
		
		
	

	}

}
