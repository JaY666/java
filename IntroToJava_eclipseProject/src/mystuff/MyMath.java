package mystuff;

public class MyMath {

	public static int max(int a, int b) {
		if (a > b) {
			return a;
		} else {
			return b;
		}
	}

	public static long max(long a, long b) {
		if (a > b) {
			return a;
		} else {
			return b;
		}
	}

	public static float max(float a, float b) {
		if (a > b) {
			return a;
		} else {
			return b;
		}
	}

	public static double max(double a, double b) {
		if (a > b) {
			return a;
		} else {
			return b;
		}
	}

	public static int min(int a, int b) {
		if (a < b) {
			return a;
		} else {
			return b;
		}
	}

	public static long min(long a, long b) {
		if (a < b) {
			return a;
		} else {
			return b;
		}
	}

	public static float min(float a, float b) {
		if (a < b) {
			return a;
		} else {
			return b;
		}
	}

	public static double min(double a, double b) {
		if (a < b) {
			return a;
		} else {
			return b;
		}
	}

	public static int abs(int a) {
		if (a > 0) {
			return a;
		} else {
			return a * -1;
		}
	}

	public static long abs(long a) {
		if (a > 0) {
			return a;
		} else {
			return a * -1;
		}
	}

	public static float abs(float a) {
		if (a > 0) {
			return a;
		} else {
			return a * -1;
		}
	}

	public static double abs(double a) {
		if (a > 0) {
			return a;
		} else {
			return a * -1;
		}
	}

	public static int pow(int a, int b) {

		int wartosc = 1;
		for (int i = 1; i <= b; i++) {
			wartosc = a * i;
		}
		return wartosc;
	}
	
	public static long pow(long a, long b) {

		long wartosc = 1;
		for (int i = 1; i <= b; i++) {
			wartosc = a * i;
		}
		return wartosc;
	}
	public static float pow(float a, float b) {

		float wartosc = 1;
		for (int i = 1; i <= b; i++) {
			wartosc = a * i;
		}
		return wartosc;
	}
	public static double pow(double a, double b) {

		double wartosc = 1;
		for (int i = 1; i <= b; i++) {
			wartosc = a * i;
		}
		return wartosc;
	}
}
