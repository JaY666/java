package function;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class Example {

	public static void main(String[] args) {

		
		Function<Integer, Double> divideByTwo = (i) -> i/2.0;
		System.out.println(divideByTwo.apply(6));
		
		Consumer<Double> doublePrinter = System.out::println;
		doublePrinter.accept(10.0);
		
		Predicate<String> longerThanFive = text -> text.length() > 5;
		System.out.println(longerThanFive.test("haha"));
		System.out.println(longerThanFive.test("hahaha"));

		
		
		
		
	}

}
