package series;

import java.util.function.Function;

public class SimpleExample {

	public static void main(String[] args) {

		
		
		
		
		
		System.out.println("Suma ci�gu: " + sum(5,SimpleExample::generateSeries));
		System.out.println("Suma ci�gu: " + sum(5, (i) -> 2*i+5));
		System.out.println("Suma ci�gu: " + sum(5, (i) -> 5));
		
	
	}
	
	
	public static int sum (int n, SeriesGenerate series){
		int result = 0;
		for(int i = 0; i<n; i++){
			
			result += series.generate(i);
		}
		return result;
	}

	
	public static int generateSeries (int i) {
		return 2*i+5;
	}
	
	
	
}
