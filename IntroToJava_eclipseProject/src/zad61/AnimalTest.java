package zad61;

public class AnimalTest {

	public static void main(String[] args) {
		Animal cat = new Cat();
		Animal dog = new Dog();
		Animal [] animals = new Animal [] {cat,dog};
		for (Animal animal : animals)
			animal.makeVoice();
	}

}
