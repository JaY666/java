package list;

import java.util.ArrayList;
import java.util.List;

public class ListCheck {

	
	public static int compare (List<Integer> list, int[] array) {
		
		int result = 0;
		
		for (int i = 0; i <array.length;i++){
			if (array[i] == list.get(i)) {
				result++;	
		}
		}
		
		return result;

	}
	
	
	
	public static void main(String[] args) {
	
		
		
	int[] tab = {4,2,2,1,5,29,3,8};
	List<Integer> numbers = new ArrayList<>();
	numbers.add(1);
	numbers.add(2);
	numbers.add(4);
	numbers.add(2);
	numbers.add(5);
	numbers.add(12);
	numbers.add(3);
	numbers.add(2);
	
	System.out.println(compare(numbers, tab));
	
}
}
