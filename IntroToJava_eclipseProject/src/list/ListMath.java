package list;

import java.util.List;

public class ListMath {
	
	public static int sum(List<Integer> numbers) {
			
		int suma = 0;
		for (int number : numbers) {
			 suma += number;
		}
		
		return suma;
		
		}
	
	public static int max(List<Integer> numbers) {
		
		int m = Integer.MIN_VALUE;
		for (int number : numbers) {
			if (m < number) {
				m = number;
				
			}				
		}	
		return m;
		
	}
	
	public static int maxIndex(List<Integer> numbers) {
		
		int index = -1;
		int maxValue = Integer.MIN_VALUE;
		for (int i = 0; i < numbers.size(); i++) {
			if (maxValue < numbers.get(i)) {
				maxValue = numbers.get(i);
				index = i;
				
			}				
		}	
		return index;
}
		
	public static double ave(List<Integer> numbers) {
		
		double suma = sum(numbers);
	
		return suma/numbers.size();
		
		}
		
	
	
	
	
		
	}


