package dontEdit.example2;

public class IfElseExample {

	public static void main(String[] args) {
		int age = 6;
		if (age < 18) {
			System.out.println("no alko");
		}else{
			System.out.println("alko ok");
		}
		System.out.println("-----------");
		
		int age2 = 26;
		if(age2 < 18) {
			System.out.println("no alko");
		}else{
			System.out.println("alko ok");
		}
		
		System.out.println("-----------");
		age = 21;
		if (age < 18) {
			System.out.println("no alko");
		} else if (age < 23) {
			System.out.println("no alco in California");
		} else {
			System.out.println("alko ok");
		}
		System.out.println("koniec");
	}

}
