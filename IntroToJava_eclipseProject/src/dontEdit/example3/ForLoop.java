package dontEdit.example3;

public class ForLoop {

	public static void main(String[] args) {
		// tworzenie tablicy 5 elementowej wype�nionej zerami
		int[] numbers = { 3, 5, 6, 78, 8, 3, -23, 5 };

		// iteracja p�tl� for
		for (int i = 0; i < numbers.length; i++) {
			System.out.println("numbers[" + i + "]=" + numbers[i]);
		}
		// dzia�a tak samo jak
		// int i=0;
		// while (i<numbers.length) {
		// 		System.out.println("numbers[" + i + "]=" + numbers[i]);
		// 		i++;
		// }

		int[] otherNunumbers = new int[5];

		// iteracja p�tl� for
		for (int i = 0; i < numbers.length; i++) {
			System.out.println("numbers[" + i + "]=" + numbers[i]);
		}
	}
}
