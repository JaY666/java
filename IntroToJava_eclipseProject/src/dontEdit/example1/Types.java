package dontEdit.example1;

public class Types {

	public static void main(String[] args) {

		//inicjalizujemy zmienne r�nych typ�w
		//konwencja nazywania camelCase - piszemyRazemKolejneWyrazyZWielkiej
		byte someByte = 5; //tworzy zmienn� typu byte o nazwie someByte, przypisuje jej warto�� 5
		short someShort = 10;
		int someInt = 1000 ;
		long someLong = 10_000_000L; //czyli po prostu 10000000
		float someFloat = 10.55f;
		double someDouble = 100000.44;
		boolean someBool = false;
		char someChar = 'a';
		
		//teraz wypisujemy warto�ci naszych zmiennych
		System.out.println("someByte: " + someByte);
		System.out.println("someShort: " + someShort);
		System.out.println("someInt: " + someInt);
		System.out.println("someLong: " + someLong);
		System.out.println("someFloat: " + someFloat);
		System.out.println("someDouble: " + someDouble);
		System.out.println("someBool: " + someBool);
		System.out.println("someChar: " + someChar);
		
		System.out.println("-------------------------");
		
		someByte = Byte.MAX_VALUE; //wpisuje do zmiennej typu byte maksymaln� warto�� jak� byte mo�e przyj��
		someShort = Short.MAX_VALUE;// najwi�kszy mo�liwy short
		someInt = Integer.MAX_VALUE;
		someLong = Long.MAX_VALUE;
		someFloat = Float.MAX_VALUE;
		someDouble = Double.MAX_VALUE;
		someBool = true;
		someChar = Character.MAX_VALUE;

		System.out.println("max byte: " + someByte);
		System.out.println("max short: " + someShort);
		System.out.println("max int: " + someInt);
		System.out.println("max long: " + someLong);
		System.out.println("max float: " + someFloat);
		System.out.println("max double: " + someDouble);
		System.out.println("max bool: " + someBool);
		System.out.println("max char: " + someChar);
		
		
		System.out.println("-------------------------");
		//co mo�e p�j�� �le z niekt�rymi typami zmiennych? (baardzo rzadko ale jednak wyst�puj�)

		//przyk�ad int overflow
		int overflowingNumber =  Integer.MAX_VALUE;
		System.out.println(overflowingNumber);//pisze du�� liczb�
		overflowingNumber++; //kt�r� jeszcze zwi�kszamy o 1
		System.out.println(overflowingNumber); //ups! Wy�wietla si� bardzo ma�a

		System.out.println("-------------------------");
		//przyk�ad b��du precyzji
		float bigNumber = 10000f;//mamy du�� liczb� zmiennoprzecinkow�
		System.out.println(bigNumber);//wszystko dzia�a jak mo�na przypuszcza�
		bigNumber = bigNumber + 0.00001f; 
		System.out.println(bigNumber);//powinno by� 10000.00001

		//dla typu double takie b��dy wyst�puj� rzadziej bo maj� wi�ksz� precyzj�
		double bigDouble = 10000;
		System.out.println(bigNumber); //oczywi�cie 10000
		bigDouble = bigDouble + 0.00001;
		System.out.println(bigDouble); //dzia�a!
		
		
	}
}
