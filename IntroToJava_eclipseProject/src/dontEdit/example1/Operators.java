package dontEdit.example1;

public class Operators {

	public static void main(String[] args) {
		int a = 2*5-3/3;//2 razy 5 - 3 podzieli� na 3 (obowi�zuje kolejno�� dzia�a�)
		System.out.println(a);//9
		
		//koniuncja czyli logiczne AND
		boolean sample = false && true; 
		System.out.println(sample);//pisze false
		
		
		System.out.println("trueOrFalse?" + (sample == true));//operator por�wnania ==
		System.out.println("trueOrFalse?" + (sample = true));//operator przypisania = przypisa� do sample warto�� true

		int i = 4;

		System.out.println("i: " + i++);// post incr +1 (wypisuje i zwi�ksza)
		System.out.println("i: " + ++i);// 1+ preincr (zwi�ksza i wypisuje)
		System.out.println("i: " + --i);
		System.out.println("i: " + i--);
		System.out.println("i: " + i);
		

		int j = 5;
		//short circuit alternative - dont do if not necessary
		boolean sample2 = true || ++j > 5;//nigdy nie wejdzie na prawo od operatora || (bo tak b�dzie true)
		System.out.println("sample2 " + sample2 + "j" + j);
		
		//| operator - do always
		sample2 = true | ++j > 5; // operator | zawsze wchodzi do ko�ca
		
		System.out.println("sample2 " + sample2 + "j" + j);
	}

}
