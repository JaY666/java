package object;

public class ArrayObjectMain {
	
	public static void main (String[] args){
		
		int[] numbers = {1,5,12,90};
		ArrayTask arrayTask = new ArrayPrinter();
		arrayTask.setData(numbers);
		arrayTask.runTask();
		
		
		ArrayTask anonymousTask = new ArrayTask() {
			
			@Override
			protected void doTask(int i) {
				System.out.println(i*i);
				
			}
		};
		
		anonymousTask.setData(numbers);
		anonymousTask.runTask();
		
	}

}
