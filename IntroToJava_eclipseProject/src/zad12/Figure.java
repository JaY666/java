package zad12;

public interface Figure {
	
	double countArea();
	
	double countCircumference();

}
