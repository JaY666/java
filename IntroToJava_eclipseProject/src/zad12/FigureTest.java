package zad12;

import java.util.Scanner;

public class FigureTest {

	public static void main(String[] args) {

		
		
		System.out.println("Wybierz figure:");
		System.out.println("1 - kwadrat");
		System.out.println("2 - ko�o");
		
		Scanner scanner = new Scanner(System.in);
        int i = scanner.nextInt();
        Figure figure = null;
        switch (i) {
            case 1 :
            	System.out.println("Podaj bok");
            	double a = scanner.nextDouble();
            	figure = new Square(a);
                break;
            case 2 :
            	System.out.println("Podaj promie�");
            	double r = scanner.nextDouble();
            	figure = new Circle(r);    
                break;
 
                
        }
        System.out.println("Pole: " + figure.countArea());
    	System.out.println("Obw�d: " + figure.countCircumference());
	}

}
