package store;

public enum Category {
	
	FOOD,
	CLOTHES,
	ELECTRONICS,
	ALCOHOL;

}
