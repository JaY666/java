package array;

import java.util.function.Consumer;

public class ArrayFunction {
	
	
	public static void doForEach (int[] tab, Consumer<Integer> task) {
		for (int i : tab) {
			task.accept(i);
			
		}

	}
	
	public static void multiplyAndPrint(int i){
		System.out.println(i*i);
	}
	
	
	public static void main(String[] args) {

		int [] numbers = {1,32,4,5};
		doForEach(numbers, System.out::println);
		
		doForEach(numbers,  (i) -> {
			System.out.println(i*i);
		});
		
		doForEach(numbers, ArrayFunction::multiplyAndPrint);
		
		
	}

}
