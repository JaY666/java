package readingFileExamples;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class WordPrinterExample {

	public static void main(String[] args) {
		
		List<String> allWords = new ArrayList<>();
		try {
			for (String line : Files.readAllLines(Paths.get("resources\\TheStory.txt"))) {
				String[] words = line.trim().split("\\W+");// dziel nie wordami
				List<String> actualWords = new ArrayList<>();
				for (String word : words) {
					//opuszczaj puste wyrazy - nie wiem sk�d si� bior� :)
					if (word.length() == 0) {
						continue;
					}
					allWords.add(word);
					
					
				}
				
			}
		} catch (IOException e) {
			System.out.println("Nie mo�na odczyta� pliku");
		}
		
		printMedianWord(allWords);
	}
	
	private static void printMedianWord (List<String> wordsInLine) {
		
		
		if (wordsInLine.isEmpty()) {
			return;
		} else if (wordsInLine.size()%2 == 1){
			
			System.out.println(wordsInLine.get(((wordsInLine.size())/2)));
			
		} else if (wordsInLine.size()%2 == 0) {
			
			System.out.println(wordsInLine.get((wordsInLine.size())/2-1) + " " + wordsInLine.get((wordsInLine.size())/2));
		}
		
		
	}
	
	

}
