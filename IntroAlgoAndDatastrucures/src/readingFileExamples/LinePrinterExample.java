package readingFileExamples;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class LinePrinterExample {
	
	public static void main(String[] args) {
		try {
			for (String line : Files.readAllLines(Paths.get("resources\\TheStory.txt"))) {
				System.out.println(line);
			}
		} catch (IOException e) {
			System.out.println("Nie mo�na odczyta� pliku");
		}
	}

}
