package collectionApiExamples.sets;

import java.util.Set;
import java.util.TreeSet;

public class Example1Sets {

	public static void main(String[] args) {
		// Set trzyma unikalne elementy - nie pozwala na duplikaty
		Set<String> set = new TreeSet<>(); // tworzy pusty Set

		set.add("Ala");
		set.add("Ala");
		set.add("Tomek");
		set.add("Krzysiek");

		// nie ma get() w secie trzeba si� iterowa� po jego elementach
		for (String str : set) {
			System.out.println(str);
		}

		// wypisuje zawarto��
		System.out.println(set);

		// wyjmuje si� podaj�c obiekt
		set.remove("Ala");
		System.out.println(set);
	}
}
