package collectionApiExamples.sets;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class Example2IteratingOverSet {
	public static void main(String[] args) {
		Set<Integer> set = new TreeSet<>(); 

		for (int i = 0; i < 10; i++) {
			set.add(i);			
		}

		for (Integer i : set) {
			if (i % 2 == 0) {
				//to nie jest dobry pomys�
				set.remove(i);
			}
		}
		System.out.println(set);

//		//w taki spos�b usuwanie zadzia�a
//		for (Iterator<Integer> it = set.iterator(); it.hasNext();) {
//			Integer element = it.next();
//			if (element % 2 == 0) {
//				//Uwaga! metoda wo�ana na iteratorze a nie na zbiorze!
//				it.remove();
//			}
//		}
//		System.out.println(set);
	}
}
