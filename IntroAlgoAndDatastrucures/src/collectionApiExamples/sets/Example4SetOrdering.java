package collectionApiExamples.sets;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

public class Example4SetOrdering {
	/**
	 * Przyk�ad pokazuj�cy w jakiej kolejno�ci uk�adaj� si� elementy w zbiorach 
	 */
	public static void main(String[] args) {
		SortedSet<String> treeSet = new TreeSet<>();
		treeSet.add("Zosia");
		treeSet.add("Marek");
		treeSet.add("Basia");
		treeSet.add("Adam");
		printOutContents(treeSet);
		
		Set<String> hashSet = new HashSet<>();
		hashSet.add("Zosia");
		hashSet.add("Marek");
		hashSet.add("Basia");
		hashSet.add("Adam");
		printOutContents(hashSet);
		
		Set<String> linkedHashSet = new LinkedHashSet<>();
		linkedHashSet.add("Zosia");
		linkedHashSet.add("Marek");
		linkedHashSet.add("Basia");
		linkedHashSet.add("Adam");
		printOutContents(linkedHashSet);
	}
	
	private static void printOutContents(Set<String> set) {
		System.out.println("SET TYPE: " + set.getClass());
		for (String str : set) {
			System.out.print(str + " ");
		}
		System.out.println();
	}
}
