package collectionApiExamples.sets;

import java.util.HashSet;
import java.util.Set;

public class Example3HashSetVsTreeSet {

public static void main(String[] args) {
		
		for (int seriesSize = 10; seriesSize <= 1_000_000; seriesSize *= 10 ) {
			Set<String> set = initSetAndMeasureTime();
			
			doSomethingAndMeasureTime(set, seriesSize);
		}
	}
	
	private static void doSomethingAndMeasureTime (Set<String> set, int amountOfWork) {
		long timeBefore = System.currentTimeMillis();
		for (int i = 0; i < amountOfWork; i++) {
//			set.add("text" + i + "_" + i);//dodawanie
//			set.remove("text" + i);
			set.contains("text " + i);//sprawdzanie czy zawiera
		}
		long millisElapsed = System.currentTimeMillis() - timeBefore;
		System.out.println("Milliseconds elapsed for " + amountOfWork + " elements: " + millisElapsed);
	}
	
	private static Set<String> initSetAndMeasureTime() {
		long timeBefore = System.currentTimeMillis();
		HashSet<String> set = new HashSet<>();
//		TreeSet<String> set = new TreeSet<>();
		for (int i = 0; i < 2_000_000; i++) {
			set.add("text " + i);
		}
		long millisElapsed = System.currentTimeMillis() - timeBefore;
		System.out.println("initialization millis: " + millisElapsed);
		return set;
	}

}
