package collectionApiExamples.otherContainers;

import java.util.PriorityQueue;

public class PriorityQueueExample {

	public static void main(String[] args) {
		
		//wsadza si� dowolnie ale wyci�ganie jest wg priorytetu
		PriorityQueue<Integer> priorityQueue = new PriorityQueue<>();
		
		priorityQueue.add(4);
		priorityQueue.add(2);
		priorityQueue.add(3);
		priorityQueue.add(1);
		priorityQueue.add(2);
		
		//peek() patrzy nie wyjmuj�c nic
		System.out.println(priorityQueue.peek());
		
		//pool() wyjmuje z kolejki
		while (!priorityQueue.isEmpty()) {
			System.out.print(priorityQueue.poll());
		}
	}

}
