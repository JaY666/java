package collectionApiExamples.otherContainers;

import java.util.LinkedList;
import java.util.Queue;

public class QueueExample {

	public static void main(String[] args) {
		 //kolejka dzia�a odwrotnie - first in first out FIFO 
		 Queue<Integer> queue = new LinkedList<>();
		 queue.add(6);
		 queue.add(7);
		 queue.add(3);
		 System.out.println(queue.poll());
		 System.out.println(queue.poll());
		 System.out.println(queue.poll());
	}

}
