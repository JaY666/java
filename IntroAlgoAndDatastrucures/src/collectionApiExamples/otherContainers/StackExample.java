package collectionApiExamples.otherContainers;

import java.util.Stack;

public class StackExample {

	public static void main(String[] args) {
		//Stack czyli stos - jak np stos talerzy - last in first out - LIFO
		Stack<Integer> stack = new Stack();
		
		//k�adzie na wierz
		stack.push(1);
		stack.push(2);
		stack.push(3);
		System.out.println(stack);
		
		//peek() odczytuje element z wierzchu stosu nie zdejmuj�c go
		System.out.println(stack.peek());
		
		//pop() zdejmuje i odczytuje element z wierzchu stosu 
		while(!stack.isEmpty()){
			System.out.print(stack.pop());
		}
	}

}
