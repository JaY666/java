package collectionApiExamples.maps;

import java.util.Map;
import java.util.TreeMap;

public class Example1Maps {

	public static void main(String[] args) {
		// Map trzyma elementy zorganizowane jako klucz -> warto�� - klucze musz� by� unikalne
		Map<String, String> map = new TreeMap<>(); // tworzy pust� Map'�

		map.put("dog", "pies");
		map.put("lock", "zamek");
		map.put("castle", "zamek");
		map.put("second", "sekunda");
		map.put("second", "drugi");
		System.out.println(map);

		// get() przyjmuje argument b�d�cy kluczem pod kt�rym warto�� chcemy obejrze�
		System.out.println(map.get("dog"));
		
		//co gdy nie ma nic pod danym kluczem?
		System.out.println(map.get("cat"));
		
		//usuwanie
		map.remove("dog");
		System.out.println(map);
	}
}
