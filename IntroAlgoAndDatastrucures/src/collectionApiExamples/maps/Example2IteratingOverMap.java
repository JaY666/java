package collectionApiExamples.maps;

import java.util.Map;
import java.util.TreeMap;

public class Example2IteratingOverMap {

	public static void main(String[] args) {
		Map<String, Integer> map = new TreeMap<>(); 
		map.put("dog", 4);
		map.put("human", 2);
		map.put("centipede", 100);
		map.put("snake", 0);
		
		// //po mapie nie mo�na si� iterowa�. Musimy wyci�gn�� jej entrySet
		for (Map.Entry<String, Integer> entry : map.entrySet()) {
			System.out.print(" k:" + entry.getKey() + " v:" + entry.getValue());
		}
		System.out.println();
		
		//mo�na te� wyci�gn�� same klucze
		for (String key : map.keySet()) {
			System.out.print(" k:" + key );
		}
		System.out.println();
		
		//albo same warto�ci
		for (Integer legsCount : map.values()) {
			System.out.print(" legs:" + legsCount);
		}
	}
}
