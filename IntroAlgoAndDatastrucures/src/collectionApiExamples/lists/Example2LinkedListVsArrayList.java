package collectionApiExamples.lists;

import java.util.ArrayList;
import java.util.List;

public class Example2LinkedListVsArrayList {

	public static void main(String[] args) {
		
		for (int seriesSize = 10; seriesSize <= 100_000; seriesSize *= 10 ) {
			List<Integer> list = initBigList();
			
			doSomethingAndMeasureTime(list, seriesSize);
		}
	}
	
	private static void doSomethingAndMeasureTime (List<Integer> list, int amountOfWork) {
		long timeBefore = System.currentTimeMillis();
		for (int i = 0; i < amountOfWork; i++) {
//			list.remove(2);//usuwanie z pocz�tku
//			list.get(amountOfWork);//wyci�ganie z dowolnego miejsca
			list.get(list.size()-1);//wyci�ganie z ko�ca
		}
		long millisElapsed = System.currentTimeMillis() - timeBefore;
		System.out.println("Milliseconds elapsed for " + amountOfWork + " elements: " + millisElapsed);
	}
	
	private static List<Integer> initBigList() {
		ArrayList<Integer> list = new ArrayList<>();
//		LinkedList<Integer> list = new LinkedList<>();
		for (int i = 0; i < 1_000_000; i++) {
			list.add(i);
		}
		return list;
	}
}
