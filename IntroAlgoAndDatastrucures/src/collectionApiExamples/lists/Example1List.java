package collectionApiExamples.lists;

import java.util.ArrayList;
import java.util.List;

public class Example1List {

	public static void main(String[] args) {
		//ArrayList dzia�a podobnie jak tablica
		List<String> list = new ArrayList<>();//tworzy pust� ArrayList�
		
		list.add("Ala");//wsadza element na koniec listy
		list.add("Tomek");
		list.add("Krzysiek");
		
		//get() wyci�ga element pod wskazanym indeksem
		System.out.println(list.get(0));
		
		// wypisuje zawarto�� ca�ej listy
		System.out.println(list);
		
		//modyfikuje element pod indeksem zero
		list.set(0, "Kasia"); 
		System.out.println(list);
		
		//mo�na doda� do �rodka listy. Wszystkie elementy po dodanym zostan� przesuni�te jeden indeks wy�ej
		list.add(2,"Zosia"); 
		System.out.println(list);
		
		//mo�na wyj�� elementy z listy. Wszystkie elementy po dodanym zostan� przesuni�te jeden indeks ni�ej
		list.remove(1); 
		System.out.println(list);
	}
}
