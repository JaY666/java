package sorting;

import java.util.Arrays;
import java.util.Random;

public class SortingExample {

	public static void main(String[] args) {
		int[] array = { 2, 5, 834, 32 };
//		int[] array = initBigArray();
		printArray(array);
		
		Arrays.sort(array);
		
		printArray(array);
	}
	
	private static void printArray(int [] arr){
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + ", ");
		}
		System.out.println();
	}
	
	private static int[] initBigArray(){
		int[] arr = new int[100_000];
		Random rand = new Random();
		for (int i = 0; i < arr.length; i++) {
			arr[i]= rand.nextInt();
		}
		return arr;
		
	}
}
