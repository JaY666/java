package math;

public class Fibo {

	
	public static int fibo(int a) {
		
		int[] tab = new int[50];
		tab[0] = 1;
		tab[1] = 1;
		
		int i = 2;
		
		while (i != (a)) {
			
			tab[i] = tab[i-1] + tab[i-2];
			i++;
			
			
		}
		return(tab[a-1]);
	}
	
	
	public static int fibo3(int a) {
		
		int[] tab = new int[3];
		tab[0] = 1;
		tab[1] = 1;
		
		
		for (int i = 2; i < a; i++) {
		
			tab[2] = tab[1] + tab[0];
			tab[0] = tab[1];
			tab[1] = tab[2];
		
		}
		
		return tab[2];
	}
	
	
	
	public static void main(String[] args) {
			
		
//		System.out.println(fibo(20));
		
		
//		System.out.println(fibo3(20));

		
		
	}

}
