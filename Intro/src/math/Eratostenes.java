package math;

import java.util.ArrayList;
import java.util.List;

public class Eratostenes {
	
	public static List<Integer> era(int n){
		List<Integer> primeNumbers = new ArrayList<Integer>();
		boolean[] tab = new boolean[n];
		
		
		
		for (int i = 0; i < n; i++) {
			tab[i] = false;
		}
		
		for (int j = 2; j < n; j++) {
			for ( int k = 2 ; k * j < n; k++) {
				tab[k*j] = true;
		}	
		}
		tab[0] = true;
		tab[1] = true;
		
		for (int i = 0; i < n; i++) {
			
			if (tab[i] == false)
				primeNumbers.add(i);
		}
		return primeNumbers;
			
			
			
		}
		
		
	
	
	
	

	public static void main(String[] args) {

		
		System.out.println(era(100));
		
		
	}

}
