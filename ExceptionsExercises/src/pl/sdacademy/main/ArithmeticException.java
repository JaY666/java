package pl.sdacademy.main;

public class ArithmeticException extends Exception {
	
	ArithmeticException(String message) {
		
		super(message);
		
	}

}
