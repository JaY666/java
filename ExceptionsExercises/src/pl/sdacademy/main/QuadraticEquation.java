package pl.sdacademy.main;

import java.util.Scanner;

public class QuadraticEquation {

	private int a;
	private int b;
	private int c;

	
	public QuadraticEquation() {}
	
	
	
	public QuadraticEquation(int a, int b, int c) {
		this.a = a;
		this.b = b;
		this.c = c;
	}

	
	
	private int getNumber(){
		System.out.println("Podaj liczbę całkowitą");
		return ReadNumbers.readInt();
		
	}
	public double[] solve() throws ArithmeticException{
		
		
		if (a == 0 & b == 0 & c == 0) {
			System.out.println("Podaj wspołczynnik do x^2");
			a = getNumber();
			System.out.println("Podaj wspołczynnik do x");
			b = getNumber();
			System.out.println("Podaj wspołczynnik do wyrazu wolnego");
			c = getNumber();
		} if (a == 0) {
			System.out.println("Wspołczynnik do x^2 musi byc różny od 0");
			System.exit(0);
		}
		
		
		double delta = Math.sqrt(b * b - 4 * a * c);
		double [] roots = new double[2];
		
		if (delta < 0) {
			throw new ArithmeticException("Delta jest mniejsza od 0");
		}
		
		roots[0] = (-b +  delta) / (2*a) ;
		roots[1] = (-b -  delta) / (2*a) ;
		
		
		
		
		return roots;
	
		
	}






	public static void main(String[] args) {

		
		QuadraticEquation q = new QuadraticEquation();
		double [] r = new double[2];
		
		try {
			r = q.solve();
		} catch (ArithmeticException e) {
			e.getMessage();
		}
		
		for (Double d : r) {
			System.out.println(d);
		}
		
		
		
	}

}
