package pl.sdacademy.main;

public class Division {

	
	public static double divide(int a, int b) throws IllegalArgumentException {
		
		if ( b == 0) {
			throw new IllegalArgumentException("Podałeś dzielną równą 0, ta liczba musi byc różna od 0");
		}
		
		return (double) a/b;
	
		
	}
	
	public static double divide(double a, double b) throws IllegalArgumentException {
		if ( b == 0) {
			throw new IllegalArgumentException("Podałeś dzielną równą 0, ta liczba musi byc różna od 0");
		}
		
		return a/b;
	
		
	}
	

	
	public static void main(String[] args) {


		System.out.println(Division.divide(5, 2));
		
		
		
	}

}
