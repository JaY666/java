package pl.sdacademy.main;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ReadNumbers {

	@SuppressWarnings("finally")
	public double readDouble() {
		
		double i = 0.0;
		try {
			Scanner sc = new Scanner(System.in);
			i = sc.nextDouble();
		
		} catch (InputMismatchException e) {
			i= 0.0;
			
		} finally {
		return i;
		}

		
	}
	
	@SuppressWarnings("finally")
	public static int readInt() {
		
		int i = 0;
		try {
			Scanner sc = new Scanner(System.in);
			i = sc.nextInt();
		
		} catch (InputMismatchException e) {
			i= 0;
			
		} finally {
		return  i;
		}
		
		
		
	}
	

	@SuppressWarnings("finally")
	public String readString() {
		
		String i = new String();
		try {
			Scanner sc = new Scanner(System.in);
			i = sc.nextLine();
		
		} catch (InputMismatchException e) {
			i = "";
			
		} finally {
		return  i;
		}
		
	}

	
	public static void main(String[] args) {
		
		
		ReadNumbers r = new ReadNumbers();
		
		System.out.println(r.readString());
	}

}
