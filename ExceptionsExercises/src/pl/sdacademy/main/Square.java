package pl.sdacademy.main;

public class Square {
	
	public static double square(int n) {
	
		if (n<0) {
			throw new IllegalArgumentException("Argument ma być wiekszy od 0");
			

		} 
		
		return Math.sqrt(n);
		
		
	}
	

	public static void main(String[] args) {

		
		Square.square(-2);
		
		
		
	}

}
