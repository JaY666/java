package pl.sdacademy.main;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Methods {
	
	private DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	private DocumentBuilder db;
	private Document doc;
	

	
	
	public double getAvgSalary(String filename) throws ParserConfigurationException, SAXException, IOException{
		
		
			
			File f = new File("recources/" + filename);
			
			db = dbf.newDocumentBuilder();
			doc = db.parse(f);
			
			doc.getDocumentElement().normalize();
			
			NodeList nList = doc.getElementsByTagName("staff");
			double avg = 0;
			
			for(int i = 0; i < nList.getLength(); i++) {
				
				Node n = nList.item(i);
				
				if(n.getNodeType() == Node.ELEMENT_NODE) {
					Element elem = (Element) n;
					
					avg += Double.parseDouble(elem.getElementsByTagName("salary").item(0).getTextContent());
				
				}
		
			}

		return avg/nList.getLength();
		

		
	}
	
	
	public List<String> getNames(String filename) throws ParserConfigurationException, SAXException, IOException {
		
		List<String> list = new ArrayList<>();
		
		
		File f = new File("recources/" + filename);
		
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.parse(f);
		
		doc.getDocumentElement().normalize();
		
		NodeList nList = doc.getElementsByTagName("staff");
		
		
		for(int i = 0; i < nList.getLength(); i++) {
			
			Node n = nList.item(i);
			
			if(n.getNodeType() == Node.ELEMENT_NODE) {
				Element elem = (Element) n;
				
				String staffName = elem.getElementsByTagName("firstname").item(0).getTextContent() 
						+ " " + elem.getElementsByTagName("lastname").item(0).getTextContent();
			  list.add(staffName);
			}
	
		}
	
		return list;
	}
	
	
	
	public void getMinMaxStaffID(String filename) throws ParserConfigurationException, SAXException, IOException {
		
		

		File f = new File("recources/" + filename);
		
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.parse(f);
		
		doc.getDocumentElement().normalize();
		
		NodeList nList = doc.getElementsByTagName("staff");
		
		
		int maxID = Integer.MIN_VALUE;
		int minID = Integer.MAX_VALUE;
		
		for(int i = 0; i < nList.getLength(); i++) {
			
			Node n = nList.item(i);
			
			if(n.getNodeType() == Node.ELEMENT_NODE) {
				Element elem = (Element) n;
	
//				maxID = Integer.parseInt(elem.getAttribute("id"));
//				minID = Integer.parseInt(elem.getAttribute("id"));
				
				if ( maxID < Integer.parseInt(elem.getAttribute("id"))) {
					
					maxID = Integer.parseInt(elem.getAttribute("id"));
					
				} 
				if ( minID > Integer.parseInt(elem.getAttribute("id"))) {
					minID = Integer.parseInt(elem.getAttribute("id"));
				}
				
			
			}
	
		}

			System.out.println("Max ID: "+ maxID + " Min ID: " + minID);
		
		
		
		
	}
	
	
public void writeXMLFileStudents (String filename) throws ParserConfigurationException, TransformerException {
		
		
		
		db = dbf.newDocumentBuilder();
		doc = db.newDocument();
		
		Element root = doc.createElement("root");
		
		Element students = doc.createElement("STUDENTS");
		
		Element student = doc.createElement("STUDENT");
		
		Element studentName = doc.createElement("Name");
		Element studentLastName = doc.createElement("LastName");
		Element studentYear = doc.createElement("Year");
		studentName.setTextContent("John");
		studentLastName.setTextContent("Simple");
		studentYear.setTextContent("3");
		
		student.appendChild(studentName);
		student.appendChild(studentLastName);
		student.appendChild(studentYear);
		
		Element student2 = doc.createElement("STUDENT");
		
		Element student2Name = doc.createElement("Name");
		Element student2LastName = doc.createElement("LastName");
		Element student2Year = doc.createElement("Year");
		student2Name.setTextContent("Jane");
		student2LastName.setTextContent("Doe");
		student2Year.setTextContent("1");
		
		student2.appendChild(student2Name);
		student2.appendChild(student2LastName);
		student2.appendChild(student2Year);
		
		students.appendChild(student);
		students.appendChild(student2);
		
		root.appendChild(students);
		
		doc.appendChild(root);
		
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer t = tf.newTransformer();
		
		DOMSource source = new DOMSource(doc);
		StreamResult sr = new StreamResult(new File("recources/" + filename));
		t.transform(source, sr);
		
		
		}
	
public void writeXMLFileCompanies (String filename) throws ParserConfigurationException, TransformerException {
	
	
	db = dbf.newDocumentBuilder();
	doc = db.newDocument();
	
	Element root = doc.createElement("root");
	
	
	
	root.appendChild(CompanyElement("Testowa", "2008", "345", "23"));
	root.appendChild(CompanyElement("Testowa 2", "1979", "34345", "40"));
	root.appendChild(CompanyElement("Testowa 3", "1999", "5", "8"));
	
	doc.appendChild(root);
	
	TransformerFactory tf = TransformerFactory.newInstance();
	Transformer t = tf.newTransformer();
	
	DOMSource source = new DOMSource(doc);
	StreamResult sr = new StreamResult(new File("recources/" + filename));
	t.transform(source, sr);
	
	
	}
	
	public Element CompanyElement (String attr, String starts, String employees, String vat) throws ParserConfigurationException {
		

		
		
		Element company = doc.createElement("COMPANY");
		
		company.setAttribute("name", attr);
		
		Element starts1 = doc.createElement("STARTS");
		starts1.setTextContent(starts);
		
		Element employees1 = doc.createElement("EMPLOYEES");
		employees1.setTextContent(employees);
		
		Element vat1 = doc.createElement("VAT");
		vat1.setTextContent(vat);
		
		company.appendChild(starts1);
		company.appendChild(employees1);
		company.appendChild(vat1);
		
		
		
		return company;
	
	}
	
	
	public double countAverage(String filename, int a, int b) throws ParserConfigurationException, SAXException, IOException {
		
		
		db = dbf.newDocumentBuilder();
		
		double sumOfsums = 0;
		double lengthOfLenghts = 0;
		for (int i = a; i<=b; i++) {
			
			double sum = 0;
			
			
			File f = new File("recources/" + filename + "_" + i + ".xml");
			doc = db.parse(f);
			doc.getDocumentElement().normalize();
			
			NodeList nList = doc.getElementsByTagName("values");
			
			
			for(int j = 0; j < nList.getLength(); j++) {
				
				Node n = nList.item(j);
				
				if(n.getNodeType() == Node.ELEMENT_NODE) {
					Element elem = (Element) n;
					
					sum += Double.parseDouble(elem.getElementsByTagName("val").item(0).getTextContent());
				
				}
		
			}
			
			lengthOfLenghts += nList.getLength();
			sumOfsums += sum;
			
		}

		return sumOfsums/lengthOfLenghts;
	}
	
	

	

}
