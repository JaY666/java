package pl.sdacademy.main;

import java.io.File;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class Person {

	private String name;
	private String lastname;
	private String company;
	private double salary;
	private String department;
	private int yearOfBorn;

	public Person() {

	}

	public Person(String name, String lastname, String company, double salary, String department, int yearOfBorn) {

		this.name = name;
		this.lastname = lastname;
		this.company = company;
		this.salary = salary;
		this.department = department;
		this.yearOfBorn = yearOfBorn;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public int getYearOfBorn() {
		return yearOfBorn;
	}

	public void setYearOfBorn(int yearOfBorn) {
		this.yearOfBorn = yearOfBorn;
	}

	public void savePeople(List<Person> people) throws ParserConfigurationException, TransformerException{
		
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.newDocument();
		
		Element root = doc.createElement("root");
		int i = 1;
		
		for ( Person p : people) {
			
			Element person = doc.createElement("Person");
			person.setAttribute("ID", i+"");
			
			
			Element name = doc.createElement("Name");
			name.setTextContent(p.getName());
			
			Element lastname = doc.createElement("Lastname");
			lastname.setTextContent(p.getLastname());
			Element company = doc.createElement("Company");
			company.setTextContent(p.getCompany());
			
			
			Element salary = doc.createElement("Salary");
			salary.setTextContent(p.getSalary()+"");
			Element department = doc.createElement("Department");
			department.setTextContent(p.getDepartment());
			Element yearOfBorn = doc.createElement("YearOfBorn");
			yearOfBorn.setTextContent(p.getYearOfBorn()+"");
			
			person.appendChild(name);
			person.appendChild(lastname);
			person.appendChild(company);
			person.appendChild(salary);
			person.appendChild(department);
			person.appendChild(yearOfBorn);
			
			
			
			
			root.appendChild(person);
			i++;
		}
		
		
		doc.appendChild(root);
		
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer t = tf.newTransformer();
		
		DOMSource source = new DOMSource(doc);
		StreamResult sr = new StreamResult(new File("recources/" + "people.xml"));
		t.transform(source, sr);
		
		
	}

}
