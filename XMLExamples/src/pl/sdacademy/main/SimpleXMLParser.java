package pl.sdacademy.main;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class SimpleXMLParser {

	
	
	
	
	public void readXMLFile (String filename) throws ParserConfigurationException, SAXException, IOException{
		
		File f = new File("recources/" + filename);
		
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.parse(f);
		
		doc.getDocumentElement().normalize();
		
		NodeList nList = doc.getElementsByTagName("staff");
		
		for(int i = 0; i < nList.getLength(); i++) {
			
			Node n = nList.item(i);
			
			if(n.getNodeType() == Node.ELEMENT_NODE) {
				Element elem = (Element) n;
				System.out.println(elem.getAttribute("id"));
				
				String staffName = elem.getElementsByTagName("firstname").item(0).getTextContent() 
						+ " " + elem.getElementsByTagName("lastname").item(0).getTextContent();
				
				System.out.println(staffName);
				System.out.println("---------------");
			}
			
			
			
		}
		
		
		
		
		
		
		
		
	}
	
	
	public void writeXMLFile (String filename) throws ParserConfigurationException, TransformerException {
		
		
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.newDocument();
		
		Element root = doc.createElement("root");
		
		Element child = doc.createElement("children");
		child.setAttribute("id", "15");
		child.setTextContent("wartosc");
		
		Element child2 = doc.createElement("children");
		child2.setAttribute("id", "16");
		child2.setTextContent("wartosc druga");
		
		root.appendChild(child);
		root.appendChild(child2);
		
		doc.appendChild(root);
		
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer t = tf.newTransformer();
		
		DOMSource source = new DOMSource(doc);
		StreamResult sr = new StreamResult(new File("recources/" + filename));
		t.transform(source, sr);
		
		
		
		
		
		
		
		
		
		
		
		
	}
}
