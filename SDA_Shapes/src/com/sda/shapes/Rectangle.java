package com.sda.shapes;

public class Rectangle implements Shape{

	private int sideA;
	private int sideB;
	
	public Rectangle(int sideA, int sideB) {
		this.sideA = sideA;
		this.sideB = sideB;
	}

	@Override
	public double area() {
		int result = sideA * sideB;
		return result;
	}

	public int getSideA() {
		return sideA;
	}

	public void setSideA(int sideA) {
		this.sideA = sideA;
	}

	public int getSideB() {
		return sideB;
	}

	public void setSideB(int sideB) {
		this.sideB = sideB;
	}

	@Override
	public double circuit() {
		int result = 2* sideA + 2 * sideB;
		return result;
	}

}
