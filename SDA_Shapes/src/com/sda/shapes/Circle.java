package com.sda.shapes;

public class Circle implements Shape {

	private double radius;

	public Circle(double radius) {
		this.radius = radius;
	}



	public double getRadius() {
		return radius;
	}



	public void setRadius(double radius) {
		this.radius = radius;
	}



	@Override
	public double area() {
		double result = radius*radius* Math.PI;
		return result;
	}


	
	@Override
	public double circuit() {
		double result = 2*radius* Math.PI;
		return result;
	}

}
