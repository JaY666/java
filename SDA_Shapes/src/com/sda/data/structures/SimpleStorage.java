package com.sda.data.structures;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.sda.shapes.Circle;
import com.sda.shapes.Rectangle;
import com.sda.shapes.Shape;
import com.sda.shapes.Square;
import com.sda.utils.ShapeCalculator;

public class SimpleStorage implements AbstractStorage{

	private  List<Shape> list;


	public SimpleStorage() {
		this.list = new ArrayList<>();
		
	}

	
	
	public void show() {
		
		for (Shape s : list) {
			
			System.out.println(s.getClass().getSimpleName() + " " + s.area());
		
		}
	
		
	}
	
	
	
	
	public void countShapeTypesOccurrences() {
		int c = 0;
		int r = 0;
		int s = 0;
		
		for (Shape sh : list) {
			
		
		if(sh.getClass().equals(Circle.class)){
			c++;
		} else if(sh.getClass().equals(Rectangle.class)){
			r++;
		} else if(sh.getClass().equals(Square.class)){
			s++;
		}
		
		
		}
		System.out.println("Square: " + s + " Rectangle: " + r + " Circle: " + c );
		
		
		
	}
	
	

	@Override
	public void addShapeToList(Shape shape) {
		list.add(shape);
	}


	@Override
	public void deleteShapeFromList(double area) {
		
		
		Iterator <Shape> iter = list.iterator();
		
		while(iter.hasNext()) {
			Shape s = iter.next();
			
			 if(s.area() == area){
				 iter.remove();
			 }
			
		}
	
	}
	
	
	
	
	
}
