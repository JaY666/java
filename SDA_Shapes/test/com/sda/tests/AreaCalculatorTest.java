package com.sda.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import com.sda.shapes.Circle;
import com.sda.shapes.Shape;
import com.sda.shapes.Square;
import com.sda.utils.ShapeCalculator;

public class AreaCalculatorTest {
	private int squareSideLength = 5;
	private int circleRadius = 1;
	
	
	
	@Test
	public void testCircleArea(){
		Circle circle = new Circle(circleRadius);
		assertTrue(ShapeCalculator.calculateArea(circle) == Math.PI);
	}
	
	@Test
	public void testRectangleArea(){
		
		
		
	}
	
	@Test
	public void testSquareArea(){

		Square square = new Square(squareSideLength);
		assertTrue(ShapeCalculator.calculateArea(square)==25);
	}

}
