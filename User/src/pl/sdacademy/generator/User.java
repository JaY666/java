package pl.sdacademy.generator;

public class User {
	
	
	private String name;
	private String lastname;
	private String adress;
	private String tel;
	private String cc;
	private String pesel;
	private UserSex sex;
	
	
	@Override
	public String toString() {
		return "User|| Imie: " + name + " Nazwisko: " + lastname + " Adres: "+ adress+ " Pesel: "+ pesel+" Tel: "+tel+" Numer karty: "+cc;
	}
	
	
	
	

	public UserSex getSex() {
		return sex;
	}




	public void setSex(UserSex sex) {
		this.sex = sex;
	}




	public String getName() {
		return name;
	}




	public void setName(String name) {
		this.name = name;
	}




	public String getLastname() {
		return lastname;
	}




	public void setLastname(String lastname) {
		this.lastname = lastname;
	}




	public String getAdress() {
		return adress;
	}




	public void setAdress(String adress) {
		this.adress = adress;
	}




	public String getTel() {
		return tel;
	}




	public void setTel(String tel) {
		this.tel = tel;
	}




	public String getCc() {
		return cc;
	}




	public void setCc(String cc) {
		this.cc = cc;
	}




	public String getPesel() {
		return pesel;
	}




	public void setPesel(String pesel) {
		this.pesel = pesel;
	}




	public User() {
		super();
	}









	public User(String name, String lastname, String adress, String tel, String cc, String pesel, UserSex sex) {
		super();
		this.name = name;
		this.lastname = lastname;
		this.adress = adress;
		this.tel = tel;
		this.cc = cc;
		this.pesel = pesel;
		this.sex = sex;
	}




	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
