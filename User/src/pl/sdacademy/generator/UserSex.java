package pl.sdacademy.generator;

public enum UserSex {
	MALE(0),FEMALE(1);
	
	
	private int sexId;
	
	public int getSexId() {
		return sexId;
	}

	public void setSexId(int sexId) {
		this.sexId = sexId;
	}

	private UserSex(int id) {
		this.sexId = id;
	}
	
	public int getId(){
		return sexId;
		
	}
	
	
	
	
}
