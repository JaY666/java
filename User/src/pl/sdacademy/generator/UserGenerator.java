package pl.sdacademy.generator;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class UserGenerator {
	
	
	private HashMap<String, ArrayList<String>> resources = new HashMap<>();
	private final List<String> files = Arrays.asList("im_f.txt","im_m.txt","miasta.txt","nazwiska.txt","ulice.txt");
	
	public boolean isMale() {
		if ( getRandomSex().equals(UserSex.MALE) ){
		return true;
		} else {
		return false;
		}
	}
	
	public String getRandomName() {
		Random r = new Random();
		
		if (isMale()) {
		return resources.get("im_m").get(r.nextInt(resources.get("im_m").size()-1));
		} else {
		return resources.get("im_f").get(r.nextInt(resources.get("im_f").size()-1));
		}
	}
	
	public String getRandomLastname() {
		
		Random r = new Random();	
		return resources.get("nazwiska").get(r.nextInt(resources.get("nazwiska").size()-1));
		
		
	}
	
	public String getRandomAdress() {
		
		Random r = new Random();
		DecimalFormat df = new DecimalFormat("00");
		DecimalFormat df2 = new DecimalFormat("000");
		
		String city = resources.get("miasta").get(r.nextInt(resources.get("miasta").size()-1));
		String street = resources.get("ulice").get(r.nextInt(resources.get("ulice").size()-1));
		 
		
		
		return street +" "+ r.nextInt(100)+", " +city+ ", " + df.format(r.nextInt(100)) +"-"+ df2.format(r.nextInt(1000));
		
	}
	
	
	public String getRandomTel() {
		
		Random r = new Random();
		DecimalFormat df = new DecimalFormat("000");
		return df.format(r.nextInt(900)+100)+" "+df.format(r.nextInt(1000))+" "+df.format(r.nextInt(1000));
		
		
	}
	
	public String getRandomCC() {
		Random r = new Random();
	
		DecimalFormat df = new DecimalFormat("0000");
		return df.format(r.nextInt(10000))+" "+df.format(r.nextInt(10000))+" "+df.format(r.nextInt(10000))+" "+df.format(r.nextInt(10000));
		
		
	}
	
	public String getRandomDateOfBirth(){
		
		int[] daysInMonth = {31,28,31,30,31,30,31,31,30,31,30,31};
		Random r = new Random();
		int year = 2017 - r.nextInt(100);
		int month = 12 - r.nextInt(12);
		
		DecimalFormat df = new DecimalFormat("00");
		
		if ( year%400 == 0) {
			daysInMonth[1] = 29;
		} if (year%4 == 0 & year%100 != 0 ) {
			daysInMonth[1] = 29;
		} 
		
		String month2 = df.format(month);
		
		String day = df.format(daysInMonth[month-1] - r.nextInt((daysInMonth[month-1])));
		
		
		String dateOfBirth = day + "/" + month2 + "/" + year;
		
		
		return dateOfBirth;
	}
	
	public String getRandomPesel() {
		String[] date = getRandomDateOfBirth().split("/");
		
		Random r = new Random();
		int last = r.nextInt(10000);
		DecimalFormat df = new DecimalFormat("0000");
		
//		if  {
//			
//		} else {
//			
//		}
		
		
		return date[2].substring(1, 3)+date[1]+date[0] + df.format(last) + r.nextInt(10);
	}

	public User getRandomUser() {
		
		User u = new User();
		u.setSex(getRandomSex());
		u.setName(getRandomName());
		u.setAdress(getRandomAdress());
		u.setLastname(getRandomLastname());
		u.setCc(getRandomCC());
		u.setTel(getRandomTel());
		u.setPesel(getRandomPesel());	
		return u;
	}
	
	
	public UserSex getRandomSex() {
		Random r = new Random();
		
			if (r.nextInt(2)%2 == 0) {
				return UserSex.FEMALE;
			} else {
				return UserSex.MALE;
			}
		
		
	}
	
	
	
	public void readFile() throws FileNotFoundException {
		for (String filename : this.files) {
			Scanner sc = new Scanner(new File("resources/" + filename));
		
			String[] myFileChoices = filename.split("\\.");
			String key = myFileChoices[0].toLowerCase();
			String myCurrentLine = "";
			ArrayList<String> list = new ArrayList<>();
			
			
			if(this.resources.get(key) == null) {
				
				
				
				while(sc.hasNext()) {
					myCurrentLine = sc.nextLine();
					list.add(myCurrentLine);
					
				}
				sc.close();
				
			}
			
			this.resources.put(key, list);
			
			
		}
		
		
		
		
		
		
		
	}

	public UserGenerator() throws FileNotFoundException {
			super();
			this.readFile();

	}
	
	
	
	
	
}
