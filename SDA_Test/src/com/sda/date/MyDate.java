package com.sda.date;

public class MyDate {
	
	private int year;
	private int month;
	private int day;
	
	private String[] strMonths = {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};
	private String[] strDays = {"Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"};
	private int[] daysInMonth = {31,28,31,30,31,30,31,31,30,31,30,31};
	
	
	public void setDate(int year, int month, int day) {
		this.year = year;
		this.month = month;
		this.day = day;
	}
	
	
	
	
	public MyDate(int year, int month, int day) {
		this.year = year;
		this.month = month;
		this.day = day;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		this.month = month;
	}
	public int getDay() {
		return day;
	}
	public void setDay(int day) {
		this.day = day;
	}


	public boolean isLeapYear(int year) {
		
		if ( year%400 == 0) {
			return true;
		} if (year%4 == 0 & year%100 != 0 ) {
			return true;
		} else {
			return false;
		}
		
	}
	
	
	public boolean isValidDate(int year, int month, int day) {
		month--;
		
		if (this.isLeapYear(year)) {
			
			daysInMonth[1] = 29;
		}
			
		if ( year >= 0 & month>= 0 & month <= 11 & day >= 0 & day <= daysInMonth[month]) {
	
			return true;
		} else {
		
		return false;
		}
	}
	
	
	
	
	public int getDayOfWeek() {
		
		return 0;
	}

	@Override
	public String toString() {
		String date = new String();
		
		date = day + " " + strMonths[month] + " " + year;
		
		return date;
	}
	
	
	
	
	
	
	
	

}
