package com.sda.time;

public class MyTime {
 private int hour;
 private int minute;
 private int second;
 
public MyTime(int hour, int minute, int second) {
	this.hour = hour;
	this.minute = minute;
	this.second = second;
}

public MyTime() {
	
}

public void setTime(int hour, int minute, int second) {
	this.hour = hour;
	this.minute = minute;
	this.second = second;

}

public int getHour() {
	return hour;
}

public void setHour(int hour) {
	this.hour = hour;
}

public int getMinute() {
	return minute;
}

public void setMinute(int minute) {
	this.minute = minute;
}

public int getSecond() {
	return second;
}

public void setSecond(int second) {
	this.second = second;
}

@Override
public String toString() {
	
	String time = new String();
	
	if (hour < 10) {
		
		time = "0" + hour + ":";
		
	} else {
		time = hour + ":";
		
	} if (minute < 10) {
		
		time = time +  "0" + minute + ":";
	} else {
		time = time + minute + ":";	
	}
	
	if (second < 10) {
		time = time +  "0" + second;
		
	} else {
		time = time + second;
	}
		
	
	return time;
}


 
 
	
	public MyTime nextSecond(){
		
		second++;
		
		if ( second == 60) {
			
			second = 0;
			this.nextMinute();
		} 

		return this;
	}
	
	
	public MyTime nextMinute(){
		
		minute++;
		
		
		if ( minute == 60) {
			
			minute = 0;
			this.nextHour();
		} 
		
		
		return this;
	}
	
	
	public MyTime nextHour(){
	
		hour++;
		
		if ( hour == 24) {
			hour = 0;
		}
	
	
	return this;
}
	
	
public MyTime previousSecond(){
		
	second--;
	
	if ( second == -1) {
		
		second = 59;
		this.previousMinute();
		}
	
		
		
		
		
		return this;
	}
	
	
	public MyTime previousMinute(){
		
		minute--;
		
		
		if ( minute == -1) {
			minute = 59;
			this.previousHour();
		} 
		
		
		return this;
	}
	
	
	public MyTime previousHour(){
	
		hour--;
		
		if ( hour == -1) {
			hour = 23;
		}
	
	
	return this;
}
	
	
	
	
	
	
	
	
}
