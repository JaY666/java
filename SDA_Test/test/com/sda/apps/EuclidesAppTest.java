package com.sda.apps;

import static org.junit.Assert.*;

import org.junit.Test;

public class EuclidesAppTest {

	
		
	@Test (expected = Exception.class)	
	public void euclides4two() throws Exception  {
			EuclidesApp.euclides4two(8, 14);
			EuclidesApp.euclides4two(6, 14);
			EuclidesApp.euclides4two(7, 14);
			assertTrue(EuclidesApp.euclides4two(8, 14)==2);
			assertTrue(EuclidesApp.euclides4two(6, 14)==2);
			assertTrue(EuclidesApp.euclides4two(-7, 14)==7);
		}
	
	@Test
	public void euclides4five () {
		
		assertTrue(EuclidesApp.euclides4five(1, 2, 3, 4, 5)==1);
		
		
	}
	

	
	
}
