package pl.sdacademy.main;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Methods {
	
	

	public static void checkIPStatuses(List<String> adresses) throws IOException {
		
		
		for (String adress : adresses) {
		
		URL obj = new URL("http://api.db-ip.com/v2/b620ee363136c97670e9054d4d2fa361c642c789/" + adress);
		
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		
		con.setRequestMethod("GET");
		con.setRequestProperty("User-Agent", "Pawel/1.0");
		
		String ret = ""; String currentLine = "";
		
		if(con.getResponseCode()==200){
			BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
			
			
			
			while((currentLine = br.readLine()) != null) {
				ret += currentLine;
			}
			br.close();
			}
		
		JSONParser parser = new JSONParser();
		try {
			JSONObject json = (JSONObject) parser.parse(ret);
			if (json.get("continentName") != null) {
			
			System.out.print("["+json.get("ipAddress")+"]");
			System.out.print("   ");
			System.out.print("Kontynent: " + json.get("continentName"));
			System.out.print("   ");
			System.out.print("Panstwo: " + json.get("countryName"));
			System.out.print("   ");
			System.out.print("Miasto: " +json.get("city"));
			System.out.println();
			}
			
		} catch (ParseException e) {
		e.printStackTrace();
		}
	
		
		}
	
		
	}
	
	
	
	
	
	
	
	
	
	public static void checkLoginStatuses(List<String> logins) throws IOException {
		
		
		for ( String login : logins) {
			
			URL obj = new URL("http://palo.ferajna.org/sda/wojciu/json.php");
			
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			
			con.setRequestMethod("POST");
			
			con.setRequestProperty("User-Agent", "Pawel/1.0");
			con.setDoOutput(true);
		
		String myParams = "login=" +login +"&haslo=qwerty";
		
		
		
		DataOutputStream dos = new DataOutputStream(con.getOutputStream());
		dos.writeBytes(myParams);
		dos.flush();
		dos.close();
		
		String ret = "";
		
		if(con.getResponseCode()==200){
			
			Scanner sc = new Scanner(new InputStreamReader(con.getInputStream()));
		
			while(sc.hasNextLine()) {
				ret += "Login: " + login +" ||"+ sc.nextLine();			}
			sc.close();
			}
		
		
		
		
		System.out.println(ret);
		}
		
		

	}
	
	
	
	
	
	public static String makeQueryURI (HashMap<String, String> map) {
		
		String result = "";
		
		for (Map.Entry<String, String> entry : map.entrySet()) {
			
		    String key = entry.getKey();
		    String value = entry.getValue();
		    
		    result += key + "=" + value + "&";
		    
		}
	
		return result.substring(0,result.length()-1);
	
	}
	
	
	
	public static HashMap<String, String> mapQueryURI(String queryURI){
		
		String[] keyEqualsValue = queryURI.split("&");
		
		HashMap<String, String> map = new HashMap<>();
		
		for ( String s : keyEqualsValue) {
			
			String[] keyValue = s.split("=");
			
				map.put(keyValue[0], keyValue[1]);
			 
			 }
		return map;
	}

	
	
	

}
