package pl.sdacademy.main;

import java.util.Scanner;

public class StringUtil {

	private String param;
		
	public StringUtil(String param) {
		this.param = param;
		
	}
	
	
	public StringUtil letterSpacing() {
		String tmp = "";
		for(int i = 0; i < this.param.length(); i++) {
			tmp += this.param.charAt(i) + " ";
			
		}
		this.param = tmp.substring(0, tmp.length() - 1);
	
		return this;
	}
	
	
	public StringUtil printString(){
		System.out.println(this.param);
		return this;
	}
	
	
	
	public StringUtil reverse() {
		
		
		for(int i = this.param.length(); i > 0; i--) {
			this.param += this.param.charAt(i-1) + "";
			
		}
		this.param = this.param.substring(this.param.length()/2, this.param.length());
	
		return this;
	}
	
	
	public StringUtil getFirstLetter() {
		
		
		this.param = this.param.substring(0, 1);
	
		return this;
	}
	
	public StringUtil insertAt(String string, int n) {
		
		
		this.param = this.param.substring(0, n).concat(string).concat(this.param.substring(n));	
	
		return this;
	}
	
	
	public StringUtil readText() {
		
		Scanner sc = new Scanner(System.in);
		
		this.param = sc.nextLine();
		
		sc.close();
		return this;
		
	}
	
	public StringUtil resetText() {
		
		for(int i = 0; i < this.param.length(); i++) {
			this.param = this.param.replace(this.param.charAt(i)+""," ");
		}
		
		return this;
	}
	
	
	public StringUtil swapLetters() {
		
		
		this.param = this.param.substring(0, 1);
		
				
		
		
		return this;
	}
	
	
	
	
	
	public StringUtil getAlphabet() {
		this.param = "";
		for(char c = 97; c < 123 ; c++) {
			this.param += c + "";
		}
		
		
		
		return this;
	}
	
	
	
}
