package pl.sdacademy.main;

public class RepoName {

	private String name;
	private String secondName;
	
	public RepoName() {}
	
	public RepoName(String name, String secondName) {
		this.name = name;
		this.secondName = secondName;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSecondName() {
		return secondName;
	}
	public void setSecondname(String secondName) {
		this.secondName = secondName;
	}
	
	public String getRepoName(String name, String secondName) {
		
		name = name.toLowerCase();
		secondName = secondName.toLowerCase();
		String repoName = new String();
		
		if (name.charAt(2) == secondName.charAt(0)) {
			
			repoName = name.substring(0, 1).toUpperCase().concat(name.substring(1, 4))
					.concat(secondName.substring(0, 1).toUpperCase().concat(secondName.substring(1, 3)))
					.concat("JA2");
			
		} else {
			
			repoName = name.substring(0, 1).toUpperCase().concat(name.substring(1, 3))
					.concat(secondName.substring(0, 1).toUpperCase().concat(secondName.substring(1, 3)))
					.concat("JA2");
			
		}
	
		return repoName;
	}
	
	
	
}
