package pl.sdacademy.main;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class LengthChecker {

	
	private static boolean isProperLength (String arg, int len) {
		
		if (arg.length() > len) {
			
			return true;
		} else {
			
			return false;
		}
	
	}
	
	public static int numberOfLines (String filename) {
		int number  = 0;
		String currentLine = "";
		try {
			Scanner sc = new Scanner(new File(ScannerIO.path + filename+ "txt"));
		
			while(sc.hasNextLine()) {
				currentLine = sc.nextLine();
				if(!currentLine.equals("")){
				number++;
				}
			}
			
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		
		return number;
	}
	
	private static String[] readFile(String filename) {
		
		
		String[] arrayString = new String[LengthChecker.numberOfLines(filename)];
		int i = 0;
		try {
			Scanner sc = new Scanner(new File(ScannerIO.path + filename+ "txt"));
		
			while(sc.hasNext()) {
				
				arrayString[i++] = sc.nextLine();;
				
			}
			
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
	
		
		
		return arrayString;
	}
	
	
	private static String[] writeFile(String[] fileContent, int len){
		
		String[] arrayString = new String[fileContent.length];
		
		int j = 0;
		for (int i=0; i < fileContent.length; i++) {
			
			if ( LengthChecker.isProperLength(fileContent[i], len)) {
				
				arrayString[j] = fileContent[i];

			}
					
			
		}	
		return arrayString;
		
	}
	
	public void make(String fileInput, int len){
		
		
		try {
			PrintWriter pw = new PrintWriter ( new FileWriter(new File (ScannerIO.path + fileInput + "_" +len +"txt")), true);
			
			 
			for(  int i =0; i < LengthChecker.numberOfLines(fileInput); i++  ){
			
			
			
			pw.println(LengthChecker.writeFile(LengthChecker.readFile(fileInput), len));
			
			
			
			}
			
			
			pw.close();
			
			
			} catch (IOException e) {
				e.printStackTrace();
			}
		
		
		
	}
	
	
	
	
	
}
