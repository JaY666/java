package pl.sdacademy.main;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Sentencer extends Sentence{
	
	public static void writeSentence(String filename, String sentence) {
		
		try {
			PrintWriter pw = new PrintWriter ( new FileWriter(new File (ScannerIO.path + filename)), true);
			pw.print(sentence);
			pw.close();
			
			
			} catch (IOException e) {
				e.printStackTrace();
			}
		
		
		
	}

}
