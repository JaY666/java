package pl.sdacademy.main;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class MoneyConverter {
	
	
	
	private static double readCourse(String currency) {
		
		String text = new String();
		double course = 0;
		int unit = 0;
		String[] currenyArray = new String[3];
		String[] currenyArrayUnit = new String[3];
		try {
			Scanner sc = new Scanner(new File(ScannerIO.path + "currency.txt"));
		
			while(sc.hasNext()) {
				
				text = sc.nextLine();
				
				if (text.contains(currency)) {
					
					currenyArray = text.split("\t");
					currenyArrayUnit = currenyArray[0].split(" ");
				}
				text = "";
				
			}
			
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		currenyArray[1] = currenyArray[1].replace(',', '.');
		
		course = Double.parseDouble(currenyArray[1]);
		unit = Integer.parseInt(currenyArrayUnit[0]);
		
		
		return course/unit;
	}
	
	
	public static double convert(double money, String to) {
		
		return money * MoneyConverter.readCourse(to);
	}
	

	
	public static double convert(double money, String to, String from){
		
	
		return money * MoneyConverter.readCourse(from)/MoneyConverter.readCourse(to);
	}
	

}
