package pl.sdacademy.main;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class TempConverter {
	
	
	public static double toKelvin(double temp){
	
		return temp + 273.15;
	}
	
	
	public static double toFahrenheit(double temp){
		
		return temp *  9/5  + 32;
	}
	
	
	public static int numberOfLines (String filename) {
		int number  = 0;
		String currentLine = "";
		try {
			Scanner sc = new Scanner(new File(ScannerIO.path + filename));
		
			while(sc.hasNextLine()) {
				currentLine = sc.nextLine();
				if(!currentLine.equals("")){
				number++;
				}
			}
			
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		
		return number;
	}
	
	
	
	
	public double[] readTemp(String filename) {
		
		double[] temps = new double[TempConverter.numberOfLines(filename)];
		
		String text = new String();
		int i = 0;

		try {
			Scanner sc = new Scanner(new File(ScannerIO.path + filename));
		
			while(sc.hasNext()) {
				
				text = sc.nextLine();
				
				text = text.replace(',', '.');
				
				temps[i++] = Double.parseDouble(text);
				
				text = "";
				
			}
			
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		return temps;
	}
			
			
	public void writeTemp(double[] temp){
		
		
		try {
			PrintWriter pw1 = new PrintWriter ( new FileWriter(new File (ScannerIO.path + "tempK.txt")), true);
			PrintWriter pw2 = new PrintWriter ( new FileWriter(new File (ScannerIO.path + "tempF.txt")), true);
			for (int i=0 ; i<temp.length; i++){
			
			
			pw1.println(TempConverter.toFahrenheit(temp[i]));
			pw2.println(TempConverter.toKelvin(temp[i]));
			
			
			}
			pw1.close();
			pw2.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		
		
		
	}
	
	
	
	
	
	

}
